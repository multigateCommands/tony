Multigate command version of https://github.com/jd7h/random-tony

A generator for chocolate flavours based on a context-free grammar.
Inspired by the limited edition Tony's Chocolonely flavours.
