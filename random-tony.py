#!/usr/bin/env python3
# coding=utf8
import sys
sys.path.insert(0, 'generator/src')
from generate_flavours import generate_tony_flavours_nl
from generate_flavours import generate_tony_flavours_en

args = sys.argv

if len(args) > 1 and args[1].lower() == "nl":
    generate_tony_flavours_nl(1)
else:
    generate_tony_flavours_en(1)
